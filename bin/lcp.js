#!/usr/bin/env node

const lcp = require('../lib/index.js');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  { name: 'port', alias: 'p', type: Number, defaultValue: 8010 },
  {
    name: 'proxyPartial',
    type: String,
    defaultValue: '/'
  },
  { name: 'proxyUrl', type: String },
  { name: 'credentials', type: Boolean, defaultValue: false },
  { name: 'origin', type: String, defaultValue: '*' }
];

try {
  const options = commandLineArgs(optionDefinitions);
  if (!options.proxyUrl) {
    throw new Error('--proxyUrl is required');
  }
  lcp.startProxy(
    options.port,
    options.proxyUrl,
    options.proxyPartial,
    options.credentials,
    options.origin
  );
} catch (error) {
  console.error(error);
}
